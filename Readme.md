Il s'agit d'un quiz portant sur la série The Office (US) pour le cours de programmation web.

Lorsque vous ouvrez le site, assurez-vous que le volume de votre ordinateur n'est pas trop élevé, car il y a une musique de fond qui se lance à l'ouverture.

Pour lancer le quiz choisissez un des trois niveaux ; en passant la souris sur les boutons "?" vous aurez des informations sur les niveaux.

À la fin de chaque niveau, votre score s'affiche avec la correction des questions.
Pour retourner au menu depuis la page des résultats, il suffit de cliquer sur le bouton "Accueil".